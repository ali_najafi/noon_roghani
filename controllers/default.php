<?php

use Carbon\Carbon;

jsonify(array(
    'error' => 0,
    'status_code' => 200,
    'msg'   => 'Framework Run Correctly @ '.Carbon::now('Asia/Tehran'),
), 200);
